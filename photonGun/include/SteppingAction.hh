#ifndef LohengrinSteppingAction_h
#define LohengrinSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include "TTree.h"
#include "TFile.h"

class G4LogicalVolume;

namespace Lohengrin
{

class EventAction;

/// Stepping action class

class SteppingAction : public G4UserSteppingAction
{
  public:
    SteppingAction(EventAction *eventAction);
    ~SteppingAction() override; // = default;

    // method from the base class
    void UserSteppingAction(const G4Step*) override;
    void SetPhotonProductionThreshold( double );
    

  private:
    EventAction *fEventAction = nullptr;
    double _photonProductionThreshold;

};

}


#endif
