#ifndef LohengrinPhysicsList_h
#define LohengrinPhysicsList_h 1

#include "G4VModularPhysicsList.hh"

namespace Lohengrin
{


class PhysicsList: public G4VModularPhysicsList
{
public:
  PhysicsList();
  ~PhysicsList() override = default;

  void SetCuts() override;
};

}

#endif

