#ifndef LohengrinRunAction_h
#define LohengrinRunAction_h 1

#include "G4UserRunAction.hh"
#include "G4Accumulable.hh"
#include "globals.hh"

#include "TFile.h"
#include "TTree.h"

class G4Run;

namespace Lohengrin
{

/// Run action class
///
/// In EndOfRunAction(), it calculates the dose in the selected volume
/// from the energy deposit accumulated via stepping and event actions.
/// The computed dose is then printed on the screen.

class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    ~RunAction() override = default;

    void BeginOfRunAction(const G4Run*) override;
    void   EndOfRunAction(const G4Run*) override;
    TTree* GetOutputTree();

  private:
    TFile *_outputFile;
    TTree *_outputTree;


};

}

#endif

