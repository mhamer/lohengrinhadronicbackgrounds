#ifndef LohengrinEventAction_h
#define LohengrinEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include <vector>

#include "TFile.h"
#include "TTree.h"

namespace Lohengrin
{

class RunAction;

/// Event action class

class EventAction : public G4UserEventAction
{
  public:
    EventAction(RunAction* runAction);
    ~EventAction() override;

    void BeginOfEventAction(const G4Event* event) override;
    void EndOfEventAction(const G4Event* event) override;

    
    void AddFSParticle( double px, double py, double pz, double E, double x, double y, double z, int ID, double edep);
    void SaveEvent();

    void FillTree();
    int GetEventID();
  
  private:
    RunAction *fRunAction;
    TTree *_outputTree;
    std::vector<int> _FSParticleID;
    std::vector<double> _FSParticlePx;
    std::vector<double> _FSParticlePy;
    std::vector<double> _FSParticlePz;
    std::vector<double> _FSParticleE;
    std::vector<double> _FSParticleX;
    std::vector<double> _FSParticleY;
    std::vector<double> _FSParticleZ;
    std::vector<double> _FSEnergyDeposit;
    int _eventID;
    bool _saveEvent;
};

}


#endif


