#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4ElementVector.hh"

namespace Lohengrin 
{


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  
  G4NistManager* nist = G4NistManager::Instance();

  G4double world_x = 1.*m;
  G4double world_y = 1.*m;
  G4double world_z = 1.*m;
  G4bool checkOverlaps = true;

  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  G4Material *Vacuum = new G4Material("vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure );


  G4Box* worldSolid = new G4Box("World", world_x, world_y, world_z );
  G4Material *worldMaterial = nist->FindOrBuildMaterial("G4_Air");
  G4LogicalVolume* worldLogical = new G4LogicalVolume(worldSolid, Vacuum, "World" );
  G4PVPlacement* worldPhysical = new G4PVPlacement( nullptr, G4ThreeVector(), worldLogical, "World", nullptr, false, 0, checkOverlaps );
 
  
  G4double target_x = 50.*cm;
  G4double target_y = 50.*cm;
  G4double target_z = 50.*cm;

  G4Box* targetSolid = new G4Box("Target", target_x, target_y, target_z );
  //G4Material* targetMaterial = nist->FindOrBuildMaterial("G4_Si");
  G4Material* targetMaterial = nist->FindOrBuildMaterial("G4_W");
  std::cout << "MATERIAL : " << targetMaterial->GetZ() << " and " << targetMaterial->GetA() << std::endl;
  std::cout << "DENSITY is " << targetMaterial->GetDensity()/(g/cm3) << std::endl;
  std::cout << "number of elements: " << targetMaterial->GetElementVector()->size() << std::endl;
  const G4Element* tE = targetMaterial->GetElementVector()->at(0);
  std::cout << tE->GetName() << std::endl;
  std::cout << "number of isotopes: " << tE->GetNumberOfIsotopes() << std::endl;
  const G4IsotopeVector* IsoVec = tE->GetIsotopeVector();
  G4double* Frac = tE->GetRelativeAbundanceVector();
  std::cout << " Frac size is " << sizeof(Frac)/sizeof(G4double) << std::endl;
  std::cout << "mole is " << mole << std::endl;
  std::cout << "Na is " << CLHEP::Avogadro << std::endl;
  for( unsigned int iI = 0; iI < IsoVec->size(); ++iI ) {
    std::cout << "-----------------" << std::endl;
    std::cout << "\t Isotope #" << iI << " with relative abundance " << Frac[iI] <<  std::endl;
    std::cout << "\t Z: " << IsoVec->at(iI)->GetZ() << std::endl;
    std::cout << std::setprecision(12) << "\t A: " << IsoVec->at(iI)->GetA()/CLHEP::Avogadro/kg << std::endl;
    std::cout << "\t A: " << IsoVec->at(iI)->GetA()/CLHEP::Avogadro/kg*CLHEP::c_light/m*s*CLHEP::c_light/m*s/1.e6/e_SI << std::endl;
    std::cout << "\t N: " << IsoVec->at(iI)->GetN() << std::endl;
    std::cout << "\t m: " << IsoVec->at(iI)->Getm()/MeV << std::endl;
  }

  //targetMaterial->GetMaterialPropertiesTable()->DumpTable();
  
  const G4ElementVector *vec = targetMaterial->GetElementVector();
  std::cout << vec << std::endl;
  std::cout << vec->size() << std::endl;
  std::cout << vec->at(0)->GetZ() << " " << vec->at(0)->GetA() << " " << vec->at(0)->GetN() << std::endl;
  //targetMaterial->GetMaterialPropertiesTable()->DumpTable();
  G4LogicalVolume *targetLogical = new G4LogicalVolume( targetSolid, targetMaterial, "Target");
  //G4LogicalVolume *targetLogical = new G4LogicalVolume( targetSolid, Vacuum, "Target");
  G4VPhysicalVolume *targetPhysical = new G4PVPlacement( 0, G4ThreeVector(0., 0., 1.*cm), targetLogical, "Target", worldLogical, false, 0 );

  

  return worldPhysical;
}


}
