#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
// #include "Run.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

namespace Lohengrin
{

RunAction::RunAction()
{
  _outputFile = new TFile("Geant4RunOutput.root", "RECREATE");
  _outputTree = new TTree("Geant4RunOutputTree", "Geant4OutputTree");
}


void RunAction::BeginOfRunAction(const G4Run*)
{

}


void RunAction::EndOfRunAction(const G4Run* run)
{

  std::cout << std::endl;
//  _outputTree->Write();
  _outputTree->Write(0, TObject::kOverwrite );
  _outputFile->Close();

}

TTree *RunAction::GetOutputTree() {
  return _outputTree;
}

}
