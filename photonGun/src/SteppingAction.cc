#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Track.hh"
#include "G4ParticleDefinition.hh"
#include "G4StepPoint.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"

#include <vector>

namespace Lohengrin
{


SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{
  _photonProductionThreshold = 0.06;
  //std::cout << "stepping action object created" << std::endl;

}

SteppingAction::~SteppingAction() {
  
}

void SteppingAction::SetPhotonProductionThreshold( double threshold ) {
  _photonProductionThreshold = threshold;
}


void SteppingAction::UserSteppingAction(const G4Step* step)
{
 

  if( fEventAction->GetEventID() != 4434 ) return;

  std::vector<double> photonX;
  std::vector<double> photonY;
  std::vector<double> photonZ;
  std::vector<double> photonPx;
  std::vector<double> photonPy;
  std::vector<double> photonPz;
  std::vector<double> photonE;
  int nPhotons = 0;
  double electronPx = 0.;
  double electronPy = 0.;
  double electronPz = 0.;
  double electronE = 0.;


  
  G4StepPoint *preStep = step->GetPreStepPoint();
  G4StepPoint *postStep = step->GetPostStepPoint();
  G4Track *track = step->GetTrack();
 
  
  std::string pname;
  if( postStep->GetProcessDefinedStep() ) {
    pname = postStep->GetProcessDefinedStep()->GetProcessName();
  }
  //if( pname == "electronNuclear" ) std::cout << " AHA!" << std::endl;
  //else return;
  std::cout << "STEP PROCESS NAME: " << pname << " in event " << std::endl; 
  
  
  fEventAction->AddFSParticle( track->GetMomentum().x(), track->GetMomentum().y(), track->GetMomentum().z(), track->GetTotalEnergy(), track->GetPosition().x(), track->GetPosition().y(), track->GetPosition().z(), track->GetParticleDefinition()->GetPDGEncoding(), step->GetTotalEnergyDeposit() );
  

  /*
  if( track->GetParticleDefinition()->GetPDGEncoding() == 11 && fabs(track->GetPosition().z() - 10.175) < std::numeric_limits<double>::epsilon() ) {
    fEventAction->AddFSParticle( track->GetMomentum().x(), track->GetMomentum().y(), track->GetMomentum().z(), track->GetTotalEnergy(), track->GetPosition().x(), track->GetPosition().y(), track->GetPosition().z(), track->GetParticleDefinition()->GetPDGEncoding() );
  }
  
  if( track->GetParticleDefinition()->GetPDGEncoding() == 22 && fabs(track->GetPosition().z() - 10.175) < std::numeric_limits<double>::epsilon() ) {
    fEventAction->AddFSParticle( track->GetMomentum().x(), track->GetMomentum().y(), track->GetMomentum().z(), track->GetTotalEnergy(), track->GetPosition().x(), track->GetPosition().y(), track->GetPosition().z(), track->GetParticleDefinition()->GetPDGEncoding() );
    if( track->GetTotalEnergy() > _photonProductionThreshold ) fEventAction->SaveEvent();
  }
  */
  /*
  if( track->GetParticleDefinition()->GetPDGEncoding() == 22 ) {
    std::cout << " for track # " << track->GetTrackID() << " found a gamma that was produced from : " << track->GetCreatorProcess()->GetProcessName() << std::endl;
  }
  */

  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "=========================" << std::endl;
  std::cout << "PRE STEP POINT: " << std::endl;
  std::cout << "  X = " << preStep->GetPosition().x() << " Y = " << preStep->GetPosition().y() << " Z = " << preStep->GetPosition().z() << std::endl;
  std::cout << " PX = " << preStep->GetMomentum().x() << " PY = " << preStep->GetMomentum().y() << " PZ = " << preStep->GetMomentum().z() << std::endl;
  std::cout << " MASS = " << preStep->GetMass() << " and Energy = " << preStep->GetTotalEnergy() << std::endl;
  std::cout << " COMPARE " << sqrt(preStep->GetTotalEnergy()*preStep->GetTotalEnergy() - preStep->GetMomentum().x()*preStep->GetMomentum().x() - preStep->GetMomentum().y()*preStep->GetMomentum().y() - preStep->GetMomentum().z()*preStep->GetMomentum().z() ) << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  
  std::cout << "------------------------" << std::endl;
  std::cout << " TRACK : " << std::endl;
  std::cout << " Track particle name : " << track->GetParticleDefinition()->GetParticleName() << " with ID " << track->GetParticleDefinition()->GetPDGEncoding() << std::endl;
  std::cout << " Track Position : " << track->GetPosition().x() << " " << track->GetPosition().y() << " " << track->GetPosition().z() << std::endl;
  std::cout << " Track Momentum : " << track->GetMomentum().x() << " " << track->GetMomentum().y() << " " << track->GetMomentum().z() << std::endl;
  std::cout << " Track Charge:    " << track->GetDynamicParticle()->GetCharge() << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;

  std::cout << "-----------------------" << std::endl;
  std::cout << "POST STEP POINT: " << std::endl;
  std::cout << "  X = " << postStep->GetPosition().x() << " Y = " << postStep->GetPosition().y() << " Z = " << postStep->GetPosition().z() << std::endl;
  std::cout << " PX = " << postStep->GetMomentum().x() << " PY = " << postStep->GetMomentum().y() << " PZ = " << postStep->GetMomentum().z() << std::endl;
  std::cout << " MASS = " << postStep->GetMass() << " and Energy = " << postStep->GetTotalEnergy() << std::endl;
  std::cout << " COMPARE " << sqrt(postStep->GetTotalEnergy()*postStep->GetTotalEnergy() - postStep->GetMomentum().x()*postStep->GetMomentum().x() - postStep->GetMomentum().y()*postStep->GetMomentum().y() - postStep->GetMomentum().z()*postStep->GetMomentum().z() ) << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "ENERGY DEPOSIT IS " << step->GetTotalEnergyDeposit();

  //std::cout << "did a step" << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
}


}
