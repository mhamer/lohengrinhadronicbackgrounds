#include "PhysicsList.hh"

#include "G4EmStandardPhysics.hh"
#include "G4EmExtraPhysics.hh"

namespace Lohengrin
{


PhysicsList::PhysicsList()
{
  defaultCutValue = 0.7*CLHEP::mm;
  SetVerboseLevel(1);
  

  RegisterPhysics( new G4EmStandardPhysics(1) );
  RegisterPhysics( new G4EmExtraPhysics(1) );

}


void PhysicsList::SetCuts()
{
  G4VUserPhysicsList::SetCuts();
}


}
