#include "EventAction.hh"
#include "RunAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

namespace Lohengrin
{

EventAction::EventAction( RunAction *runAction )
: fRunAction(runAction)
{
  _outputTree = fRunAction->GetOutputTree();

  _outputTree->Branch("EventID", &_eventID );
  _outputTree->Branch("FSParticleID", &_FSParticleID );
  _outputTree->Branch("FSParticlePx", &_FSParticlePx );
  _outputTree->Branch("FSParticlePy", &_FSParticlePy );
  _outputTree->Branch("FSParticlePz", &_FSParticlePz );
  _outputTree->Branch("FSParticleE", &_FSParticleE );
  _outputTree->Branch("FSParticleX", &_FSParticleX );
  _outputTree->Branch("FSParticleY", &_FSParticleY );
  _outputTree->Branch("FSParticleZ", &_FSParticleZ );
  _outputTree->Branch("FSEnergyDeposit", &_FSEnergyDeposit );
}

EventAction::~EventAction() {

}


void EventAction::BeginOfEventAction(const G4Event* event)
{
  //if( event->GetEventID()%100000 == 0 ) { 
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "STARTING EVENT #" << event->GetEventID() << "             \r" << std::endl; //; fflush(stdout); //std::endl;
  //}
  _eventID = event->GetEventID();
  _FSParticleID.clear();
  _FSParticlePx.clear();
  _FSParticlePy.clear();
  _FSParticlePz.clear();
  _FSParticleE.clear();
  _FSParticleX.clear();
  _FSParticleY.clear();
  _FSParticleZ.clear();
  _FSEnergyDeposit.clear();
  //_saveEvent = false;
  _saveEvent = true; //false;
}

void EventAction::SaveEvent() {
  _saveEvent = true;
} 

void EventAction::EndOfEventAction(const G4Event* event)
{
  if( _saveEvent ) _outputTree->Fill();

  //std::cout << std::endl << std::endl << std::endl << std::endl << std::endl;

}

void EventAction::AddFSParticle( double px, double py, double pz, double E, double x, double y, double z, int ID, double edep ) {
  _FSParticleID.push_back( ID );
  _FSParticlePx.push_back( px );
  _FSParticlePy.push_back( py );
  _FSParticlePz.push_back( pz );
  _FSParticleE.push_back( E );
  _FSParticleX.push_back( x );
  _FSParticleY.push_back( y );
  _FSParticleZ.push_back( z );
  _FSEnergyDeposit.push_back( edep );
}

int EventAction::GetEventID() {
  return _eventID;
}

}
