#include <iostream>

#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"
#include "PhysicsList.hh"
#include "FTFP_BERT.hh"
#include "G4EmExtraPhysics.hh"
#include "myFTFP_BERT.hh"

#include "G4RunManagerFactory.hh"

using namespace Lohengrin;

int main( int argc, char** argv ) {


  long rseed = 124;
  if( argc > 1 ) rseed = std::atol(argv[1]);
  
  G4long seed = rseed; //124;
  std::cout << "setting seed to " << seed << std::endl;
  G4Random::setTheSeed(seed);
  auto runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::SerialOnly);

  runManager->SetUserInitialization(new DetectorConstruction());
  auto physicsList = new FTFP_BERT; //new myFTFP_BERT; //PhysicsList;
  runManager->SetUserInitialization(physicsList);
  runManager->SetUserInitialization(new ActionInitialization() );


  runManager->Initialize();

  int numberOfEvents = 5000;
  runManager->BeamOn(numberOfEvents);

  delete runManager;


  return 0;

}
